package com.tilldeathcoder.todoist.util.color;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("modules.common.util")
class TodoistColorFindServiceTest {

    private final TodoistColorFindService todoistColorFindService = new TodoistColorFindService();

    @Test
    @DisplayName("TodoistColorFindService findAll() returns all colors")
    void findAll_ok() {
        // given
        List<TodoistColors> expected = stream(TodoistColors.values()).collect(toList());

        // when
        List<TodoistColors> actual = todoistColorFindService.findAll();

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findByIds() returns all required colors")
    void findByIds_valid_ids_ok() {
        // given
        List<TodoistColors> expected = asList(TodoistColors.RED, TodoistColors.BLUE, TodoistColors.GREEN);

        // when
        List<TodoistColors> actual = todoistColorFindService.findByIds(31, 41, 36);

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findByIds() with empty ids returns all colors")
    void findByIds_empty_ids_ok() {
        // given
        List<TodoistColors> expected = stream(TodoistColors.values()).collect(toList());

        // when
        List<TodoistColors> actual = todoistColorFindService.findByIds();

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findByIds() with not existing id throws exception")
    void findByIds_not_existing_id_exception() {
        // given
        int notExistingColorId = 22;

        // then
        assertThrows(TodoistColorNotFoundException.class, () -> todoistColorFindService.findByIds(31, notExistingColorId, 30));
    }

    @Test
    @DisplayName("TodoistColorFindService findById() with existing id returns required color")
    void findById_existing_id_ok() {
        // given
        int existingId = TodoistColors.RED.getId();
        TodoistColors expected = TodoistColors.RED;

        // when
        TodoistColors actual = todoistColorFindService.findById(existingId);

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findById() with not existing id throws exception")
    void findById_not_existing_id_exception() {
        // given
        int notExistingId = 1;

        // then
        assertThrows(TodoistColorNotFoundException.class, () -> todoistColorFindService.findById(notExistingId));
    }

    @Test
    @DisplayName("TodoistColorFindService findByIdOptional() with existing id returns required color as Optional.class")
    void findByIdOptional_existing_id_ok() {
        // given
        int existingId = TodoistColors.RED.getId();
        Optional<TodoistColors> expected = of(TodoistColors.RED);

        // when
        Optional<TodoistColors> actual = todoistColorFindService.findByIdOptional(existingId);

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findByIdOptional() with not existing id returns Optional.empty()")
    void findByIdOptional_not_existing_id_empty() {
        // given
        int notExistingId = 1;

        // when
        Optional<TodoistColors> actual = todoistColorFindService.findByIdOptional(notExistingId);

        // then
        assertTrue(actual.isEmpty());
    }

    @Test
    @DisplayName("TodoistColorFindService findByName() with existing name returns required color")
    void findByName_existing_name_ok() {
        // given
        String existingName = TodoistColors.RED.getName();
        TodoistColors expected = TodoistColors.RED;

        // when
        TodoistColors actual = todoistColorFindService.findByName(existingName);

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findByName() with not existing name throws exception")
    void findByName_not_existing_name_exception() {
        // given
        String notExistingName = "not_existing_color_name";

        // then
        assertThrows(TodoistColorNotFoundException.class, () -> todoistColorFindService.findByName(notExistingName));
    }

    @Test
    @DisplayName("TodoistColorFindService findByNameOptional() with existing name returns required color as Optional.class")
    void findByNameOptional_existing_name_ok() {
        // given
        String existingName = TodoistColors.RED.getName();
        Optional<TodoistColors> expected = of(TodoistColors.RED);

        // when
        Optional<TodoistColors> actual = todoistColorFindService.findByNameOptional(existingName);

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findByNameOptional() with not existing name returns Optional.empty()")
    void findByNameOptional_not_existing_name_empty() {
        // given
        String notExistingName = "not_existing_color_name";

        // when
        Optional<TodoistColors> actual = todoistColorFindService.findByNameOptional(notExistingName);

        // then
        assertTrue(actual.isEmpty());
    }

    @Test
    @DisplayName("TodoistColorFindService findByHexadecimal() with existing hexadecimal returns required color")
    void findByHexadecimal_existing_hexadecimal_ok() {
        // given
        String existingHexadecimal = TodoistColors.RED.getHexadecial();
        TodoistColors expected = TodoistColors.RED;

        // when
        TodoistColors actual = todoistColorFindService.findByHexadecimal(existingHexadecimal);

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findByHexadecimal() with not existing hexadecimal throws exception")
    void findByHexadecimal_not_existing_hexadecimal_exception() {
        // given
        String notExistingHexadecimal = "not_existing_color_hexadecimal";

        // then
        assertThrows(TodoistColorNotFoundException.class, () -> todoistColorFindService.findByHexadecimal(notExistingHexadecimal));
    }

    @Test
    @DisplayName("TodoistColorFindService findByHexadecimalOptional() with existing hexadecimal returns required color as Optional.class")
    void findByHexadecimalOptional_existing_hexadecimal_ok() {
        // given
        String existingHexadecimal = TodoistColors.RED.getHexadecial();
        Optional<TodoistColors> expected = of(TodoistColors.RED);

        // when
        Optional<TodoistColors> actual = todoistColorFindService.findByHexadecimalOptional(existingHexadecimal);

        // then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("TodoistColorFindService findByHexadecimalOptional() with not existing hexadecimal returns Optional.empty()")
    void findByHexadecimalOptional_not_existing_hexadecimal_empty() {
        // given
        String notExistingHexadecimal = "not_existing_color_hexadecimal";

        // when
        Optional<TodoistColors> actual = todoistColorFindService.findByHexadecimalOptional(notExistingHexadecimal);

        // then
        assertTrue(actual.isEmpty());
    }

}