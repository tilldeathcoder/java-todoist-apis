package com.tilldeathcoder.todoist.util.color;

import lombok.Getter;

@Getter
public enum TodoistColors {
    BERRY_RED(30, "berry_red", "#b8256f"),
    RED(31, "red", "#db4035"),
    ORANGE(32, "orange", "#ff9933"),
    YELLOW(33, "yellow", "#fad000"),
    OLIVE_GREEN(34, "olive_green", "#afb83b"),
    LIME_GREEN(35, "lime_green", "#7ecc49"),
    GREEN(36, "green", "#299438"),
    MINT_GREEN(37, "mint_green", "#6accbc"),
    TEAL(38, "teal", "#158fad"),
    SKY_BLUE(39, "sky_blue", "#14aaf5"),
    LIGHT_BLUE(40, "light_blue", "#96c3eb"),
    BLUE(41, "blue", "#4073ff"),
    GRAPE(42, "grape", "#884dff"),
    VIOLET(43, "violet", "#af38eb"),
    LAVENDER(44, "lavender", "#eb96eb"),
    MAGENTA(45, "magenta", "#e05194"),
    SALMON(46, "salmon", "#ff8d85"),
    CHARCOAL(47, "charcoal", "#808080"),
    GREY(48, "grey", "#b8b8b8"),
    TAUPE(49, "taupe", "#ccac93");

    TodoistColors(int id, String name, String hexadecial) {
        this.id = id;
        this.name = name;
        this.hexadecial = hexadecial;
    }

    private final int id;
    private final String name;
    private final String hexadecial;

}
