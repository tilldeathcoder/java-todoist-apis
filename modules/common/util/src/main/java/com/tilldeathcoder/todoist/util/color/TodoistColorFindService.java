package com.tilldeathcoder.todoist.util.color;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.stream;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

public final class TodoistColorFindService {

    private static final List<TodoistColors> ALL_COLORS = unmodifiableList(stream(TodoistColors.values()).collect(toList()));

    public List<TodoistColors> findAll() {
        return ALL_COLORS;
    }

    public List<TodoistColors> findByIds(int... ids) {
        if (ids.length == 0) {
            return ALL_COLORS;
        }
        List<Integer> idsList = stream(ids).boxed().collect(toList());
        return idsList.stream().map(this::findById).collect(toList());
    }

    public TodoistColors findById(int id) {
        return ALL_COLORS.stream().filter(color -> color.getId() == id).findAny().orElseThrow(() -> new TodoistColorNotFoundException("id", id));
    }

    public Optional<TodoistColors> findByIdOptional(int id) {
        return ALL_COLORS.stream().filter(color -> color.getId() == id).findAny();
    }

    public TodoistColors findByName(String name) {
        return ALL_COLORS.stream().filter(color -> color.getName().equalsIgnoreCase(name)).findAny().orElseThrow(() -> new TodoistColorNotFoundException("name", name));
    }

    public Optional<TodoistColors> findByNameOptional(String name) {
        return ALL_COLORS.stream().filter(color -> color.getName().equalsIgnoreCase(name)).findAny();
    }

    public TodoistColors findByHexadecimal(String hexadecimal) {
        return ALL_COLORS.stream().filter(color -> color.getHexadecial().equalsIgnoreCase(hexadecimal)).findAny().orElseThrow(() -> new TodoistColorNotFoundException("hexadecimal", hexadecimal));
    }

    public Optional<TodoistColors> findByHexadecimalOptional(String hexadecimal) {
        return ALL_COLORS.stream().filter(color -> color.getHexadecial().equalsIgnoreCase(hexadecimal)).findAny();
    }
}
