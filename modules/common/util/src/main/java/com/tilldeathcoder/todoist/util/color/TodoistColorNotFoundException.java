package com.tilldeathcoder.todoist.util.color;

import static java.lang.String.format;

public final class TodoistColorNotFoundException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Color was not found. Qualifier - %s. Value - %s";

    public TodoistColorNotFoundException(String qualifier, Object value) {
        super(format(MESSAGE_TEMPLATE, qualifier, value));
    }
}
