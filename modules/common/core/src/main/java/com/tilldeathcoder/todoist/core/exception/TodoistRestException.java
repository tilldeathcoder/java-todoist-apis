package com.tilldeathcoder.todoist.core.exception;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public final class TodoistRestException extends RuntimeException {

    private final LocalDateTime timestamp;
    private final Integer status;
    private final String error;
    private final String message;
    private final String todoistResponse;

}
