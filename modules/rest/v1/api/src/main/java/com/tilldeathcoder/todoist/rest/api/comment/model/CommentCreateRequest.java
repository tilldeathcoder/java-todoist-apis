package com.tilldeathcoder.todoist.rest.api.comment.model;


import com.tilldeathcoder.todoist.rest.api.comment.model.attachment.Attachment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public final class CommentCreateRequest {

    /**
     * Required if field projectId is missing
     */
    private Long taskId;

    /**
     * Required if field taskId is missing
     */
    private Long projectId;

    /**
     * Required
     */
    private String content;
    private Attachment attachment;

}
