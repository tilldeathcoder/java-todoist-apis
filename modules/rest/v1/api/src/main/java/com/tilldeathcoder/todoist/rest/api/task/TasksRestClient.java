package com.tilldeathcoder.todoist.rest.api.task;

import com.tilldeathcoder.todoist.rest.api.task.model.TaskCreateRequest;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskResponse;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskUpdateRequest;

import java.util.Collection;
import java.util.Optional;

/**
 * Tasks resource client. REST API v1 documentation <a href="https://developer.todoist.com/rest/v1/#tasks"><b>here<b/></a>
 *
 * @author tilldeathcoder
 * @see TaskResponse
 * @see TaskCreateRequest
 * @see TaskUpdateRequest
 * @see com.tilldeathcoder.todoist.rest.api.TodoistRestClient
 */
public interface TasksRestClient {


    /**
     * This method returns all active tasks. <a href="https://developer.todoist.com/rest/v1/#get-active-tasks"><b>Resource description</b></a>
     *
     * @return - {@link Collection} of {@link TaskResponse} or should return empty {@link Collection} if no tasks were found
     */
    Collection<TaskResponse> getAllActive();

    /**
     * This method returns all active tasks filtered by project's id. <a href="https://developer.todoist.com/rest/v1/#get-active-tasks"><b>Resource description</b></a>
     *
     * @param projectId - the project's id
     * @return - {@link Collection} of {@link TaskResponse} or should return empty {@link Collection} if no tasks were found
     */
    Collection<TaskResponse> getAllActiveByProjectId(final long projectId);

    /**
     * This method returns all active tasks filtered by section's id. <a href="https://developer.todoist.com/rest/v1/#get-active-tasks"><b>Resource description</b></a>
     *
     * @param sectionId - the section's id
     * @return - {@link Collection} of {@link TaskResponse} or should return empty {@link Collection} if no tasks were found
     */
    Collection<TaskResponse> getAllActiveBySectionId(final long sectionId);

    /**
     * This method returns all active tasks filtered by label's id. <a href="https://developer.todoist.com/rest/v1/#get-active-tasks"><b>Resource description</b></a>
     *
     * @param labelId - the label's id
     * @return - {@link Collection} of {@link TaskResponse} or should return empty {@link Collection} if no tasks were found
     */
    Collection<TaskResponse> getAllActiveByLabelId(final long labelId);

    /**
     * This method returns all active tasks filtered by provided filter. <a href="https://developer.todoist.com/rest/v1/#get-active-tasks"><b>Resource description</b></a>
     *
     * @param filter - the search filter
     * @return - {@link Collection} of {@link TaskResponse} or should return empty {@link Collection} if no tasks were found
     */
    Collection<TaskResponse> getAllActiveByFilter(final String filter);

    /**
     * This method returns all active tasks filtered by provided filter and its language. <a href="https://developer.todoist.com/rest/v1/#get-active-tasks"><b>Resource description</b></a>
     *
     * @param filter - the search filter
     * @param lang   - the search filter's language
     * @return - {@link Collection} of {@link TaskResponse} or should return empty {@link Collection} if no tasks were found
     */
    Collection<TaskResponse> getAllActiveByFilterAndLang(final String filter, final String lang);

    /**
     * This method returns all active tasks by provided ids. <a href="https://developer.todoist.com/rest/v1/#get-active-tasks"><b>Resource description</b></a>
     *
     * @param ids - the tasks ids
     * @return - {@link Collection} of {@link TaskResponse} or should return empty {@link Collection} if no tasks were found
     */
    Collection<TaskResponse> getAllActiveByIds(final long[] ids);

    /**
     * This method returns task by provided id. <a href="https://developer.todoist.com/rest/v1/#get-an-active-task"><b>Resource description</b></a>
     *
     * @param id - the task's id
     * @return - {@link TaskResponse} object or should throw exception if no resource was found
     */
    TaskResponse getById(final long id);

    /**
     * This method returns task by provided id wrapped in {@link Optional}. <a href="https://developer.todoist.com/rest/v1/#get-an-active-task"><b>Resource description</b></a>
     *
     * @param id - the task's id
     * @return - {@link Optional} with {@link TaskResponse}
     */
    Optional<TaskResponse> getByIdOptional(final long id);

    /**
     * This method creates a task. <a href="https://developer.todoist.com/rest/v1/#create-a-new-task"><b>Resource description</b></a>
     *
     * @param request - {@link TaskCreateRequest} object. Should be validated! See {@link TaskCreateRequest} docs or resource official docs
     * @return - {@link TaskResponse} with created task
     */
    TaskResponse create(final TaskCreateRequest request);

    /**
     * This method updates a task by id. <a href="https://developer.todoist.com/rest/v1/#update-a-task"><b>Resource description</b></a>
     *
     * @param id      - the task's id which you want to update
     * @param request - {@link TaskUpdateRequest} object
     */
    void updateById(final long id, final TaskUpdateRequest request);

    /**
     * This method deletes a task by provided id. <a href="https://developer.todoist.com/rest/v1/#delete-a-task"><b>Resource description</b></a>
     *
     * @param id - the task's id which you want to delete
     */
    void deleteById(long id);
}
