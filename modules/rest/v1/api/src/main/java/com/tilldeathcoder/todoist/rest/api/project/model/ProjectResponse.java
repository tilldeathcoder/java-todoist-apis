package com.tilldeathcoder.todoist.rest.api.project.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString
@EqualsAndHashCode
@SuperBuilder
public final class ProjectResponse {

    private final long id;
    private final long syncId;
    private final long parentId;
    /**
     * @deprecated
     */
    @Deprecated(since = "v1")
    private final long parent;
    private final int color;
    private final int order;
    private final int commentCount;
    private final boolean shared;
    private final boolean favorite;
    private final boolean inboxProject;
    private final boolean teamInbox;
    private final String name;
    private final String url;

}
