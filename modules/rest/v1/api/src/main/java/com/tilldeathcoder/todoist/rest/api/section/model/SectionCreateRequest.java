package com.tilldeathcoder.todoist.rest.api.section.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public final class SectionCreateRequest {

    /**
     * Required
     */
    private String name;
    /**
     * Required
     */
    private long projectId;
    private Integer order;
}
