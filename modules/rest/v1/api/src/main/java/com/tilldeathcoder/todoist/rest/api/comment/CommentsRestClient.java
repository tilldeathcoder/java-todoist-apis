package com.tilldeathcoder.todoist.rest.api.comment;

import com.tilldeathcoder.todoist.rest.api.comment.model.CommentCreateRequest;
import com.tilldeathcoder.todoist.rest.api.comment.model.CommentResponse;
import com.tilldeathcoder.todoist.rest.api.comment.model.CommentUpdateRequest;
import com.tilldeathcoder.todoist.rest.api.comment.model.ProjectCommentResponse;
import com.tilldeathcoder.todoist.rest.api.comment.model.TaskCommentResponse;

import java.util.Collection;
import java.util.Optional;

/**
 * Comments resource client. REST API v1 documentation <a href="https://developer.todoist.com/rest/v1/#comments"><b>here<b/></a>
 *
 * @author tilldeathcoder
 * @see CommentResponse
 * @see ProjectCommentResponse
 * @see TaskCommentResponse
 * @see CommentCreateRequest
 * @see CommentUpdateRequest
 * @see com.tilldeathcoder.todoist.rest.api.TodoistRestClient
 */
public interface CommentsRestClient {

    /**
     * This method returns all project's comments. <a href="https://developer.todoist.com/rest/v1/#get-all-comments"><b>Resource description</b></a>
     *
     * @param projectId - the project's id
     * @return - {@link Collection} of {@link ProjectCommentResponse} or should return empty {@link Collection} if no comments were found
     */
    Collection<ProjectCommentResponse> getAllByProjectId(final long projectId);

    /**
     * This method returns all task's comments. <a href="https://developer.todoist.com/rest/v1/#get-all-comments"><b>Resource description</b></a>
     *
     * @param taskId - the task's id
     * @return - {@link Collection} of {@link TaskCommentResponse} or should return empty {@link Collection} if no comments were found
     */
    Collection<TaskCommentResponse> getAllByTaskId(final long taskId);

    /**
     * This method returns comment by provided id. <a href="https://developer.todoist.com/rest/v1/#get-a-comment"><b>Resource description</b></a>
     *
     * @param id - the comment's id
     * @return - {@link CommentResponse} object or should throw exception if no resource was found
     */
    CommentResponse getById(final long id);

    /**
     * This method returns comment by provided id wrapped in {@link Optional}. <a href="https://developer.todoist.com/rest/v1/#get-a-comment"><b>Resource description</b></a>
     *
     * @param id - the comment's id
     * @return - {@link Optional} with {@link CommentResponse}
     */
    Optional<CommentResponse> getByIdOptional(final long id);

    /**
     * This method creates a comment. <a href="https://developer.todoist.com/rest/v1/#create-a-new-comment"><b>Resource description</b></a>
     *
     * @param request - {@link CommentCreateRequest} object. Should be validated! See {@link CommentCreateRequest} docs or resource official docs
     * @return - {@link CommentResponse} with created comment
     */
    CommentResponse create(final CommentCreateRequest request);

    /**
     * This method updates a comment by provided id. <a href="https://developer.todoist.com/rest/v1/#update-a-comment"><b>Resource description</b></a>
     *
     * @param id      - the comment's id which you want to update
     * @param request - {@link CommentUpdateRequest} object. Should be validated! See {@link CommentUpdateRequest} docs or resource official docs
     */
    void updateById(final long id, final CommentUpdateRequest request);

    /**
     * This method deletes a comment by provided id. <a href="https://developer.todoist.com/rest/v1/#delete-a-comment"><b>Resource description</b></a>
     *
     * @param id - the comment's id which you want to delete
     */
    void deleteById(final long id);

}
