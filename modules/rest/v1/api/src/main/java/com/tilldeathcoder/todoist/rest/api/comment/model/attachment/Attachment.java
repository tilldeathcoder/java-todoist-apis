package com.tilldeathcoder.todoist.rest.api.comment.model.attachment;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

// TODO fix
@Getter
@ToString
@EqualsAndHashCode
@SuperBuilder
public class Attachment {

    private final String fileName;
    private final String fileSize;
    private final String fileType;
    private final String fileUrl;
    private final String resourceType;
    private final String uploadState;

}
