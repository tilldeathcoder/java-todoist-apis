package com.tilldeathcoder.todoist.rest.api.task.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString
@EqualsAndHashCode
@SuperBuilder
public final class TaskResponse {

    private final long id;
    private final long projectId;
    private final long sectionId;
    private final long parentId;
    /**
     * @deprecated
     */
    @Deprecated(since = "v1")
    private final long parent;
    private final long assignee;
    private final long assigner;
    private final long creator;
    private final long[] labelIds;
    private final int order;
    private final int priority;
    private final int commentCount;
    private final boolean completed;
    private final String url;
    private final String content;
    private final String description;
    private final String created;
    private final TaskDue due;
}
