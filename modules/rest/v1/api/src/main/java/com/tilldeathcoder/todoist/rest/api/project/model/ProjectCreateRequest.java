package com.tilldeathcoder.todoist.rest.api.project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public final class ProjectCreateRequest {

    /**
     * required
     */
    private String name;
    private Long parentId;
    /**
     * @deprecated
     */
    @Deprecated(since = "v1")
    private Long parent;
    private Integer color;
    private Boolean favorite;

}
