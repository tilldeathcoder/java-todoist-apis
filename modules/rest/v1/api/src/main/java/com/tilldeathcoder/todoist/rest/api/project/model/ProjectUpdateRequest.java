package com.tilldeathcoder.todoist.rest.api.project.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public final class ProjectUpdateRequest {

    private String name;
    private Integer color;
    private Boolean favorite;

}
