package com.tilldeathcoder.todoist.rest.api.collaborator;

import com.tilldeathcoder.todoist.rest.api.collaborator.model.CollaboratorResponse;

import java.util.Collection;

/**
 * Project's collaborators resource client. REST API v1 documentation <a href="https://developer.todoist.com/rest/v1/#get-all-collaborators"><b>here<b/></a>
 *
 * @author tilldeathcoder
 * @see CollaboratorResponse
 * @see com.tilldeathcoder.todoist.rest.api.project.ProjectsRestClient
 */
public interface CollaboratorsRestClient {

    /**
     * This method returns all project's collaborators. <a href="https://developer.todoist.com/rest/v1/#get-all-collaborators"><b>Resource description</b></a>
     *
     * @param projectId - the project's id from what you want to get collaborators
     * @return - {@link Collection} of {@link CollaboratorResponse}. Should return empty {@link Collection} if no collaborators were found and throw exception if project doesn't exist
     */
    Collection<CollaboratorResponse> getAllFromProject(final long projectId);

}
