package com.tilldeathcoder.todoist.rest.api.comment.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString
@EqualsAndHashCode
@SuperBuilder
public abstract class CommentResponse {

    private final long id;
    private final String posted;
    private final String content;

}
