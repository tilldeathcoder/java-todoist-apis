package com.tilldeathcoder.todoist.rest.api.section;

import com.tilldeathcoder.todoist.rest.api.section.model.SectionCreateRequest;
import com.tilldeathcoder.todoist.rest.api.section.model.SectionResponse;
import com.tilldeathcoder.todoist.rest.api.section.model.SectionUpdateRequest;

import java.util.Collection;
import java.util.Optional;

/**
 * Sections resource client. REST API v1 documentation <a href="https://developer.todoist.com/rest/v1/#sections"><b>here<b/></a>
 *
 * @author tilldeathcoder
 * @see SectionResponse
 * @see SectionCreateRequest
 * @see SectionUpdateRequest
 * @see com.tilldeathcoder.todoist.rest.api.TodoistRestClient
 */
public interface SectionsRestClient {

    /**
     * This method returns all sections. <a href="https://developer.todoist.com/rest/v1/#get-all-sections"><b>Resource description</b></a>
     *
     * @return - {@link Collection} of {@link SectionResponse} or should return empty {@link Collection} if no sections were found
     */
    Collection<SectionResponse> getAll();

    /**
     * This method returns project's sections. <a href="https://developer.todoist.com/rest/v1/#get-all-sections"><b>Resource description</b></a>
     *
     * @param projectId - thi project's id
     * @return - {@link Collection} of {@link SectionResponse} or should return empty {@link Collection} if no sections were found
     */
    Collection<SectionResponse> getAllByProjectId(final long projectId);

    /**
     * This method returns section by provided id. <a href="https://developer.todoist.com/rest/v1/#get-a-section"><b>Resource description</b></a>
     *
     * @param id - the section's id
     * @return - {@link SectionResponse} object or should throw exception if no resource was found
     */
    SectionResponse getById(final long id);

    /**
     * This method returns section by provided id wrapped in {@link Optional}. <a href="https://developer.todoist.com/rest/v1/#get-a-section"><b>Resource description</b></a>
     *
     * @param id - the section's id
     * @return - {@link Optional} with {@link SectionResponse}
     */
    Optional<SectionResponse> getByIdOptional(final long id);

    /**
     * This method creates a section. <a href="https://developer.todoist.com/rest/v1/#create-a-new-section"><b>Resource description</b></a>
     *
     * @param request - {@link SectionCreateRequest} object. Should be validated! See {@link SectionCreateRequest} docs or resource official docs
     * @return - {@link SectionResponse} with created section
     */
    SectionResponse create(final SectionCreateRequest request);

    /**
     * This method updates a section by id. <a href="https://developer.todoist.com/rest/v1/#update-a-section"><b>Resource description</b></a>
     *
     * @param id      - the section's id which you want to update
     * @param request - {@link SectionUpdateRequest} object. Should be validated! See {@link SectionUpdateRequest} docs or resource official docs
     */
    void updateById(final long id, final SectionUpdateRequest request);


    /**
     * This method deletes a section by provided id. <a href="https://developer.todoist.com/rest/v1/#delete-a-section"><b>Resource description</b></a>
     *
     * @param id - the section's id which you want to delete
     */
    void deleteById(final long id);
}
