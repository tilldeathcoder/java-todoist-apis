package com.tilldeathcoder.todoist.rest.api.task.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString
@EqualsAndHashCode
@SuperBuilder
public final class TaskDue {

    private final boolean recurring;
    private final String string;
    private final String date;
    private final String dateTime;
    private final String timezone;

}
