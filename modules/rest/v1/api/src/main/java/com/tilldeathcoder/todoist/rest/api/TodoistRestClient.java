package com.tilldeathcoder.todoist.rest.api;

import com.tilldeathcoder.todoist.rest.api.comment.CommentsRestClient;
import com.tilldeathcoder.todoist.rest.api.label.LabelsRestClient;
import com.tilldeathcoder.todoist.rest.api.project.ProjectsRestClient;
import com.tilldeathcoder.todoist.rest.api.section.SectionsRestClient;
import com.tilldeathcoder.todoist.rest.api.task.TasksRestClient;

/**
 * Todoist REST API v1 Java client. You can find full Todoist REST API v1 documentation <a href="https://developer.todoist.com/rest/v1/#overview"><b>here<b/></a>
 *
 * @author tilldeathcoder
 * @see CommentsRestClient
 * @see LabelsRestClient
 * @see ProjectsRestClient
 * @see SectionsRestClient
 * @see TasksRestClient
 */
public interface TodoistRestClient {

    CommentsRestClient comments();

    LabelsRestClient labels();

    ProjectsRestClient projects();

    SectionsRestClient sections();

    TasksRestClient tasks();
}
