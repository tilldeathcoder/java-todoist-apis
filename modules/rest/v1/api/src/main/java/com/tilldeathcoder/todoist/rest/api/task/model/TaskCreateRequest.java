package com.tilldeathcoder.todoist.rest.api.task.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public final class TaskCreateRequest {

    /**
     * Required
     */
    private String content;
    private String description;

    /**
     * Only one due* field can be not empty. dueLang is special case
     */
    private String dueString;

    /**
     * Only one due* field can be not empty. dueLang is special case
     */
    private String dueDate;

    /**
     * Only one due* field can be not empty. dueLang is special case
     */
    private String dueDateTime;
    private String dueLang;
    private Long projectId;
    private Long sectionId;
    private Long parentId;
    /**
     * @deprecated
     */
    @Deprecated(since = "v1")
    private Long parent;
    private Long assignee;
    private Integer order;
    private Integer priority;
    private long[] labelIds;
}
