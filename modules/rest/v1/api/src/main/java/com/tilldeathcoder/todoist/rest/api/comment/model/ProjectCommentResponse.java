package com.tilldeathcoder.todoist.rest.api.comment.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public final class ProjectCommentResponse extends CommentResponse {

    private final long projectId;

}
