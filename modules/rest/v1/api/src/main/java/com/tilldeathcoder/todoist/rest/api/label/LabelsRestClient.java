package com.tilldeathcoder.todoist.rest.api.label;

import com.tilldeathcoder.todoist.rest.api.label.model.LabelCreateRequest;
import com.tilldeathcoder.todoist.rest.api.label.model.LabelResponse;
import com.tilldeathcoder.todoist.rest.api.label.model.LabelUpdateRequest;

import java.util.Collection;
import java.util.Optional;

/**
 * Labels resource client. REST API v1 documentation <a href="https://developer.todoist.com/rest/v1/#labels"><b>here<b/></a>
 *
 * @author tilldeathcoder
 * @see LabelResponse
 * @see LabelCreateRequest
 * @see LabelUpdateRequest
 * @see com.tilldeathcoder.todoist.rest.api.TodoistRestClient
 */
public interface LabelsRestClient {

    /**
     * This method returns all labels. <a href="https://developer.todoist.com/rest/v1/#get-all-comments"><b>Resource description</b></a>
     *
     * @return - {@link Collection} of {@link LabelResponse} or should return empty {@link Collection} if no labels were found
     */
    Collection<LabelResponse> getAll();

    /**
     * This method returns label by provided id. <a href="https://developer.todoist.com/rest/v1/#get-a-label"><b>Resource description</b></a>
     *
     * @param id - the label's id
     * @return - {@link LabelResponse} object or should throw exception if no resource was found
     */
    LabelResponse getById(final long id);

    /**
     * This method returns label by provided id wrapped in {@link Optional}. <a href="https://developer.todoist.com/rest/v1/#get-a-label"><b>Resource description</b></a>
     *
     * @param id - the label's id
     * @return - {@link Optional} with {@link LabelResponse}
     */
    Optional<LabelResponse> getByIdOptional(final long id);

    /**
     * This method creates a label. <a href="https://developer.todoist.com/rest/v1/#create-a-new-label"><b>Resource description</b></a>
     *
     * @param request - {@link LabelCreateRequest} object. Should be validated! See {@link LabelCreateRequest} docs or resource official docs
     * @return - {@link LabelResponse} with created label
     */
    LabelResponse create(final LabelCreateRequest request);

    /**
     * This method updates a label by provided id. <a href="https://developer.todoist.com/rest/v1/#update-a-label"><b>Resource description</b></a>
     *
     * @param id      - the label's id which you want to update
     * @param request - {@link LabelUpdateRequest} object
     */
    void updateById(final long id, final LabelUpdateRequest labelUpdateRequest);

    /**
     * This method deletes a label by provided id. <a href="https://developer.todoist.com/rest/v1/#delete-a-label"><b>Resource description</b></a>
     *
     * @param id - the label's id which you want to delete
     */
    void deleteById(final long id);
}
