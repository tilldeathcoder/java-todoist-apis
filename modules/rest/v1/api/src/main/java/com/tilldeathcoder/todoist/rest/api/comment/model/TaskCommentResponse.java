package com.tilldeathcoder.todoist.rest.api.comment.model;


import com.tilldeathcoder.todoist.rest.api.comment.model.attachment.Attachment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public final class TaskCommentResponse extends CommentResponse {

    private final long taskId;
    private final Attachment attachment;

}
