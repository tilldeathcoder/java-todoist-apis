package com.tilldeathcoder.todoist.rest.api.task.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public final class TaskUpdateRequest {

    private String content;
    private String description;
    private String dueString;
    private String dueDate;
    private String dueDateTime;
    private String dueLang;
    private Integer priority;
    private Long assignee;
    private long[] labelIds;

}
