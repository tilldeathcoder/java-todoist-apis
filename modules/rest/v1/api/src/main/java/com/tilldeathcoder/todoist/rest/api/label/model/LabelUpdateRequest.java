package com.tilldeathcoder.todoist.rest.api.label.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public final class LabelUpdateRequest {

    private String name;
    private Boolean favorite;
    private Integer order;
    private Integer color;

}
