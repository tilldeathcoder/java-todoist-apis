package com.tilldeathcoder.todoist.rest.api.section.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString
@EqualsAndHashCode
@SuperBuilder
public final class SectionResponse {

    private final long id;
    private final long projectId;
    private final int order;
    private final String name;
}
