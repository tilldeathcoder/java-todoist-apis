package com.tilldeathcoder.todoist.rest.api.comment.model.attachment;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public final class ImageAttachment extends Attachment {

    private final String image;
    private final int imageHeight;
    private final int imageWidth;

}
