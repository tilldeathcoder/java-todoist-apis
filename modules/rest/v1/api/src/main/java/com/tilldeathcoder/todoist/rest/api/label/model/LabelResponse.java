package com.tilldeathcoder.todoist.rest.api.label.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString
@EqualsAndHashCode
@SuperBuilder
public final class LabelResponse {

    private final long id;
    private final int order;
    private final int color;
    private final boolean favorite;
    private final String name;

}
