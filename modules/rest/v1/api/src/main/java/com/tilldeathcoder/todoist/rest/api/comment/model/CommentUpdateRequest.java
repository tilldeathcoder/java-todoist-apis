package com.tilldeathcoder.todoist.rest.api.comment.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public final class CommentUpdateRequest {

    /**
     * Required
     */
    private String content;

}
