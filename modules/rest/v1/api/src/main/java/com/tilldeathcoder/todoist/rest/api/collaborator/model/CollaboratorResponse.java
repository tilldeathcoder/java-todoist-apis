package com.tilldeathcoder.todoist.rest.api.collaborator.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@ToString
@EqualsAndHashCode
@SuperBuilder
public final class CollaboratorResponse {

    private final long id;
    private final String name;
    private final String email;

}
