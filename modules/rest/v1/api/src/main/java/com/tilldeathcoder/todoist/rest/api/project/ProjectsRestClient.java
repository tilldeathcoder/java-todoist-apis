package com.tilldeathcoder.todoist.rest.api.project;

import com.tilldeathcoder.todoist.rest.api.collaborator.CollaboratorsRestClient;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectCreateRequest;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectResponse;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectUpdateRequest;

import java.util.Collection;
import java.util.Optional;

/**
 * Projects resource client. REST API v1 documentation <a href="https://developer.todoist.com/rest/v1/#projects"><b>here<b/></a>
 *
 * @author tilldeathcoder
 * @see ProjectResponse
 * @see ProjectCreateRequest
 * @see ProjectUpdateRequest
 * @see CollaboratorsRestClient
 * @see com.tilldeathcoder.todoist.rest.api.TodoistRestClient
 */
public interface ProjectsRestClient {

    /**
     * This method returns all projects. <a href="https://developer.todoist.com/rest/v1/#get-all-projects"><b>Resource description</b></a>
     *
     * @return - {@link Collection} of {@link ProjectResponse} or should return empty {@link Collection} if no projects were found
     */
    Collection<ProjectResponse> getAll();

    /**
     * This method returns project by provided id. <a href="https://developer.todoist.com/rest/v1/#get-a-project"><b>Resource description</b></a>
     *
     * @param id - the project's id
     * @return - {@link ProjectResponse} object or should throw exception if no resource was found
     */
    ProjectResponse getById(final long id);

    /**
     * This method returns project by provided id wrapped in {@link Optional}. <a href="https://developer.todoist.com/rest/v1/#get-a-project"><b>Resource description</b></a>
     *
     * @param id - the project's id
     * @return - {@link Optional} with {@link ProjectResponse}
     */
    Optional<ProjectResponse> getByIdOptional(final long id);

    /**
     * This method creates a project. <a href="https://developer.todoist.com/rest/v1/#create-a-new-project"><b>Resource description</b></a>
     *
     * @param request - {@link ProjectCreateRequest} object. Should be validated! See {@link ProjectCreateRequest} docs or resource official docs
     * @return - {@link ProjectResponse} with created project
     */
    ProjectResponse create(final ProjectCreateRequest request);

    /**
     * This method updates a project by id. <a href="https://developer.todoist.com/rest/v1/#update-a-project"><b>Resource description</b></a>
     *
     * @param id      - the project's id which you want to update
     * @param request - {@link ProjectUpdateRequest} object
     */
    void updateById(final long id, final ProjectUpdateRequest request);

    /**
     * This method deletes a project by provided id. <a href="https://developer.todoist.com/rest/v1/#delete-a-project"><b>Resource description</b></a>
     *
     * @param id - the project's id which you want to delete
     */
    void deleteById(final long id);

    /**
     * This method returns project's collaborators client
     *
     * @return {@link CollaboratorsRestClient} object
     */
    CollaboratorsRestClient collaborators();
}
