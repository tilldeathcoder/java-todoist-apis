package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.collaborator;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.tilldeathcoder.todoist.rest.api.collaborator.model.CollaboratorResponse;

import java.io.IOException;

public class CollaboratorResponseJsonDeserializer extends StdDeserializer<CollaboratorResponse> {

    public CollaboratorResponseJsonDeserializer() {
        super(CollaboratorResponse.class);
    }

    @Override
    public CollaboratorResponse deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode root = parser.getCodec().readTree(parser);
        return CollaboratorResponse.builder()
                .id(root.get("id").asInt())
                .email(root.get("email").asText())
                .name(root.get("name").asText())
                .build();
    }
}
