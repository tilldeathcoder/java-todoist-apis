package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.label;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tilldeathcoder.todoist.rest.api.label.model.LabelCreateRequest;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.AbstractJsonDeserializer;

import java.io.IOException;

public class LabelCreateRequestJsonSerializer extends AbstractJsonDeserializer<LabelCreateRequest> {

    public LabelCreateRequestJsonSerializer() {
        super(LabelCreateRequest.class);
    }

    @Override
    public void serialize(LabelCreateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeStringField("name", value.getName());
        writeBooleanFieldIfNotNull(generator, "favorite", value.getFavorite());
        writeIntegerFieldIfNotNull(generator, "order", value.getOrder());
        writeIntegerFieldIfNotNull(generator, "color", value.getColor());
        generator.writeEndObject();
    }
}
