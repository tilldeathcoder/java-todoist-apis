package com.tilldeathcoder.todoist.rest.implementation.spring.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

import static java.util.Objects.nonNull;

public abstract class AbstractJsonDeserializer<T> extends StdSerializer<T> {

    protected AbstractJsonDeserializer(Class<T> t) {
        super(t);
    }

    protected void writeStringFieldIfNotEmpty(JsonGenerator generator, String fieldName, String value) throws IOException {
        if (nonNull(value) && value.length() > 0) {
            generator.writeStringField(fieldName, value);
        }
    }

    protected void writeBooleanFieldIfNotNull(JsonGenerator generator, String fieldName, Boolean value) throws IOException {
        if (nonNull(value)) {
            generator.writeBooleanField(fieldName, value);
        }
    }

    protected void writeIntegerFieldIfNotNull(JsonGenerator generator, String fieldName, Integer value) throws IOException {
        if (nonNull(value)) {
            generator.writeNumberField(fieldName, value);
        }
    }

    protected void writeLongFieldIfNotNull(JsonGenerator generator, String fieldName, Long value) throws IOException {
        if (nonNull(value)) {
            generator.writeNumberField(fieldName, value);
        }
    }

    protected void writeLongArrayFieldIfNotNull(JsonGenerator generator, String fieldName, long[] value) throws IOException {
        if (nonNull(value)) {
            generator.writeFieldName(fieldName);
            generator.writeArray(value, 0, value.length);
        }
    }
}
