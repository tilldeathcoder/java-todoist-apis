package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.label;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.tilldeathcoder.todoist.rest.api.label.model.LabelResponse;

import java.io.IOException;

public class LabelResponseJsonDeserializer extends StdDeserializer<LabelResponse> {

    public LabelResponseJsonDeserializer() {
        super(LabelResponse.class);
    }

    @Override
    public LabelResponse deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode root = parser.getCodec().readTree(parser);

        return LabelResponse.builder()
                .id(root.get("id").asLong())
                .order(root.get("order").asInt())
                .color(root.get("color").asInt())
                .favorite(root.get("favorite").asBoolean())
                .name(root.get("name").asText())
                .build();
    }
}
