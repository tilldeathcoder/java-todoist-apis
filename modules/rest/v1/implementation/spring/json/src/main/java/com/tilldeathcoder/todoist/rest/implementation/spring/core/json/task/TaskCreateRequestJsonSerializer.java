package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.task;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskCreateRequest;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.AbstractJsonDeserializer;

import java.io.IOException;

public class TaskCreateRequestJsonSerializer extends AbstractJsonDeserializer<TaskCreateRequest> {

    public TaskCreateRequestJsonSerializer() {
        super(TaskCreateRequest.class);
    }

    @Override
    public void serialize(TaskCreateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeStringField("content", value.getContent());
        writeStringFieldIfNotEmpty(generator, "description", value.getDescription());
        writeLongFieldIfNotNull(generator, "project_id", value.getProjectId());
        writeLongFieldIfNotNull(generator, "section_id", value.getSectionId());
        writeLongFieldIfNotNull(generator, "parent_id", value.getParentId());
        writeLongFieldIfNotNull(generator, "parent", value.getParent());
        writeIntegerFieldIfNotNull(generator, "order", value.getOrder());
        writeLongArrayFieldIfNotNull(generator, "label_ids", value.getLabelIds());
        writeIntegerFieldIfNotNull(generator, "priority", value.getPriority());
        writeStringFieldIfNotEmpty(generator, "due_string", value.getDueString());
        writeStringFieldIfNotEmpty(generator, "due_date", value.getDueDate());
        writeStringFieldIfNotEmpty(generator, "due_datetime", value.getDueDateTime());
        writeStringFieldIfNotEmpty(generator, "due_lang", value.getDueLang());
        writeLongFieldIfNotNull(generator, "assignee", value.getAssignee());
        generator.writeEndObject();
    }
}
