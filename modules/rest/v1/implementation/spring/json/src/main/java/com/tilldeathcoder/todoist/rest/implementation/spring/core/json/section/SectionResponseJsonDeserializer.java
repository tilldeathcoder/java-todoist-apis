package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.section;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.tilldeathcoder.todoist.rest.api.section.model.SectionResponse;

import java.io.IOException;

public class SectionResponseJsonDeserializer extends StdDeserializer<SectionResponse> {

    public SectionResponseJsonDeserializer() {
        super(SectionResponse.class);
    }

    @Override
    public SectionResponse deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode root = parser.getCodec().readTree(parser);

        return SectionResponse.builder()
                .id(root.get("id").asLong())
                .projectId(root.get("project_id").asLong())
                .order(root.get("order").asInt())
                .name(root.get("name").asText())
                .build();
    }
}
