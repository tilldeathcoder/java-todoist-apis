package com.tilldeathcoder.todoist.rest.implementation.spring.core.json;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.tilldeathcoder.todoist.rest.api.collaborator.model.CollaboratorResponse;
import com.tilldeathcoder.todoist.rest.api.comment.model.CommentCreateRequest;
import com.tilldeathcoder.todoist.rest.api.comment.model.CommentResponse;
import com.tilldeathcoder.todoist.rest.api.comment.model.CommentUpdateRequest;
import com.tilldeathcoder.todoist.rest.api.label.model.LabelCreateRequest;
import com.tilldeathcoder.todoist.rest.api.label.model.LabelResponse;
import com.tilldeathcoder.todoist.rest.api.label.model.LabelUpdateRequest;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectCreateRequest;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectResponse;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectUpdateRequest;
import com.tilldeathcoder.todoist.rest.api.section.model.SectionCreateRequest;
import com.tilldeathcoder.todoist.rest.api.section.model.SectionUpdateRequest;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskCreateRequest;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskResponse;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskUpdateRequest;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.collaborator.CollaboratorResponseJsonDeserializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.comment.CommentCreateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.comment.CommentResponseJsonDeserializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.comment.CommentUpdateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.label.LabelCreateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.label.LabelResponseJsonDeserializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.label.LabelUpdateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.project.ProjectCreateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.project.ProjectResponseJsonDeserializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.project.ProjectUpdateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.section.SectionCreateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.section.SectionUpdateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.task.TaskCreateRequestJsonSerializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.task.TaskResponseJsonDeserializer;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.task.TaskUpdateRequestJsonSerializer;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public final class TodoistModule extends SimpleModule {

    private static final String MODULE_NAME = "TodoistModule";

    public TodoistModule() {
        super(MODULE_NAME);
        initSerializers();
        initDeserializers();
    }

    private void initSerializers() {
        addSerializer(CommentCreateRequest.class, new CommentCreateRequestJsonSerializer());
        addSerializer(CommentUpdateRequest.class, new CommentUpdateRequestJsonSerializer());

        addSerializer(LabelCreateRequest.class, new LabelCreateRequestJsonSerializer());
        addSerializer(LabelUpdateRequest.class, new LabelUpdateRequestJsonSerializer());

        addSerializer(ProjectCreateRequest.class, new ProjectCreateRequestJsonSerializer());
        addSerializer(ProjectUpdateRequest.class, new ProjectUpdateRequestJsonSerializer());

        addSerializer(SectionCreateRequest.class, new SectionCreateRequestJsonSerializer());
        addSerializer(SectionUpdateRequest.class, new SectionUpdateRequestJsonSerializer());

        addSerializer(TaskCreateRequest.class, new TaskCreateRequestJsonSerializer());
        addSerializer(TaskUpdateRequest.class, new TaskUpdateRequestJsonSerializer());
    }

    private void initDeserializers() {
        addDeserializer(CollaboratorResponse.class, new CollaboratorResponseJsonDeserializer());
        addDeserializer(CommentResponse.class, new CommentResponseJsonDeserializer());
        addDeserializer(LabelResponse.class, new LabelResponseJsonDeserializer());
        addDeserializer(ProjectResponse.class, new ProjectResponseJsonDeserializer());
        addDeserializer(TaskResponse.class, new TaskResponseJsonDeserializer());
    }
}
