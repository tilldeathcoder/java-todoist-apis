package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.section;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.tilldeathcoder.todoist.rest.api.section.model.SectionUpdateRequest;

import java.io.IOException;

public class SectionUpdateRequestJsonSerializer extends StdSerializer<SectionUpdateRequest> {

    public SectionUpdateRequestJsonSerializer() {
        super(SectionUpdateRequest.class);
    }

    @Override
    public void serialize(SectionUpdateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeStringField("name", value.getName());
        generator.writeEndObject();
    }
}
