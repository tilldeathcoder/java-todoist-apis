package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.comment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.tilldeathcoder.todoist.rest.api.comment.model.CommentUpdateRequest;

import java.io.IOException;

public class CommentUpdateRequestJsonSerializer extends StdSerializer<CommentUpdateRequest> {

    public CommentUpdateRequestJsonSerializer() {
        super(CommentUpdateRequest.class);
    }

    @Override
    public void serialize(CommentUpdateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeStringField("content", value.getContent());
        generator.writeEndObject();
    }
}
