package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.comment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.tilldeathcoder.todoist.rest.api.comment.model.CommentCreateRequest;
import com.tilldeathcoder.todoist.rest.api.comment.model.attachment.Attachment;

import java.io.IOException;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class CommentCreateRequestJsonSerializer extends StdSerializer<CommentCreateRequest> {

    public CommentCreateRequestJsonSerializer() {
        super(CommentCreateRequest.class);
    }

    @Override
    public void serialize(CommentCreateRequest value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        jsonGenerator.writeStartObject();
        writeIdField(value, jsonGenerator);
        jsonGenerator.writeStringField("content", value.getContent());
        writeAttachmentObject(value, jsonGenerator);

        jsonGenerator.writeEndObject();
    }

    private void writeIdField(CommentCreateRequest value, JsonGenerator gen) throws IOException {
        Long projectId = value.getProjectId();
        if (isNull(projectId)) {
            gen.writeNumberField("task_id", value.getTaskId());
        } else {
            gen.writeNumberField("project_id", value.getProjectId());
        }
    }

    private void writeAttachmentObject(CommentCreateRequest value, JsonGenerator jsonGenerator) throws IOException {
        Attachment attachment = value.getAttachment();
        if (nonNull(attachment)) {
            jsonGenerator.writeFieldName("attachment");
            jsonGenerator.writeStartObject();
            // TODO WIP
            jsonGenerator.writeEndObject();
        }
    }
}
