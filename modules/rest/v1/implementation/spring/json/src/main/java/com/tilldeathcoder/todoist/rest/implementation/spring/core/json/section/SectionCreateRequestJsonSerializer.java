package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.section;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tilldeathcoder.todoist.rest.api.section.model.SectionCreateRequest;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.AbstractJsonDeserializer;

import java.io.IOException;

public class SectionCreateRequestJsonSerializer extends AbstractJsonDeserializer<SectionCreateRequest> {

    public SectionCreateRequestJsonSerializer() {
        super(SectionCreateRequest.class);
    }

    @Override
    public void serialize(SectionCreateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeStringField("name", value.getName());
        generator.writeNumberField("project_id", value.getProjectId());
        writeIntegerFieldIfNotNull(generator, "order", value.getOrder());
        generator.writeEndObject();
    }
}
