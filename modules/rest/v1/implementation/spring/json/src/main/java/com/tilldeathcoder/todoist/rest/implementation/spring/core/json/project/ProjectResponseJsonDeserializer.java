package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.project;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectResponse;

import java.io.IOException;

public class ProjectResponseJsonDeserializer extends StdDeserializer<ProjectResponse> {

    public ProjectResponseJsonDeserializer() {
        super(ProjectResponse.class);
    }

    @Override
    public ProjectResponse deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode root = parser.getCodec().readTree(parser);

        return ProjectResponse.builder()
                .id(root.get("id").asLong())
                .name(root.get("name").asText())
                .color(root.get("color").asInt())
                .parentId(root.get("parent_id").asLong())
                .parent(root.get("parent").asLong())
                .order(root.get("order").asInt())
                .commentCount(root.get("comments_count").asInt())
                .shared(root.get("shared").asBoolean())
                .favorite(root.get("favorite").asBoolean())
                .inboxProject(root.get("inbox_project").asBoolean())
                .teamInbox(root.get("team_inbox").asBoolean())
                .syncId(root.get("sync_id").asLong())
                .url(root.get("url").asText())
                .build();
    }
}
