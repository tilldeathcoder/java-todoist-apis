package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.project;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectUpdateRequest;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.AbstractJsonDeserializer;

import java.io.IOException;

public class ProjectUpdateRequestJsonSerializer extends AbstractJsonDeserializer<ProjectUpdateRequest> {

    public ProjectUpdateRequestJsonSerializer() {
        super(ProjectUpdateRequest.class);
    }

    @Override
    public void serialize(ProjectUpdateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        writeStringFieldIfNotEmpty(generator, "name", value.getName());
        writeIntegerFieldIfNotNull(generator, "color", value.getColor());
        writeBooleanFieldIfNotNull(generator, "favorite", value.getFavorite());
        generator.writeEndObject();
    }
}
