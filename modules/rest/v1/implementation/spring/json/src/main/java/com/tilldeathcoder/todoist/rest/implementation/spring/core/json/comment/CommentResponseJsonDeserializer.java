package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.comment;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.tilldeathcoder.todoist.rest.api.comment.model.CommentResponse;
import com.tilldeathcoder.todoist.rest.api.comment.model.ProjectCommentResponse;
import com.tilldeathcoder.todoist.rest.api.comment.model.TaskCommentResponse;
import com.tilldeathcoder.todoist.rest.api.comment.model.attachment.Attachment;

import java.io.IOException;

import static java.util.Objects.isNull;

public class CommentResponseJsonDeserializer extends StdDeserializer<CommentResponse> {

    public CommentResponseJsonDeserializer() {
        super(CommentResponse.class);
    }

    @Override
    public CommentResponse deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode root = parser.getCodec().readTree(parser);

        JsonNode taskId = root.get("task_id");
        if (isNull(taskId)) {
            return ProjectCommentResponse.builder()
                    .id(root.get("id").asLong())
                    .projectId(root.get("project_id").asLong())
                    .content(root.get("content").asText())
                    .posted(root.get("posted").asText())
                    .build();
        }

        return TaskCommentResponse.builder()
                .id(root.get("id").asLong())
                .taskId(taskId.asLong())
                .content(root.get("content").asText())
                .posted(root.get("posted").asText())
                .attachment(deserializeAttachment(root))
                .build();
    }

    private Attachment deserializeAttachment(JsonNode root) {
        if (root.hasNonNull("attachment")) {
            JsonNode attachmentNode = root.get("attachment");
            return Attachment.builder().build();
        }
        return null;
    }
}
