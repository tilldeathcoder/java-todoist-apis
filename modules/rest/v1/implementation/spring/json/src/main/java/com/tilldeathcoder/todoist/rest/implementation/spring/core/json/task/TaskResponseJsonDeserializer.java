package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.task;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskDue;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskResponse;

import java.io.IOException;
import java.util.Iterator;

import static java.util.stream.Stream.generate;

public class TaskResponseJsonDeserializer extends StdDeserializer<TaskResponse> {

    public TaskResponseJsonDeserializer() {
        super(TaskResponse.class);
    }

    @Override
    public TaskResponse deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode root = parser.getCodec().readTree(parser);

        return TaskResponse.builder()
                .id(root.get("id").asLong())
                .projectId(root.get("project_id").asLong())
                .sectionId(root.get("section_id").asLong())
                .content(root.get("content").asText())
                .description(root.get("description").asText())
                .completed(root.get("completed").asBoolean())
                .labelIds(deserializeLabelIds(root))
                .parentId(root.get("parent_id").asLong())
                .parent(root.get("parent").asLong())
                .order(root.get("order").asInt())
                .priority(root.get("priority").asInt())
                .due(deserializeDue(root))
                .url(root.get("url").asText())
                .commentCount(root.get("comment_count").asInt())
                .assignee(root.get("assignee").asLong())
                .assigner(root.get("assigner").asLong())
                .build();
    }

    private TaskDue deserializeDue(JsonNode root) {
        if (root.hasNonNull("due")) {
            JsonNode dueNode = root.get("due");
            return TaskDue.builder()
                    .string(dueNode.get("string").asText())
                    .date(dueNode.get("date").asText())
                    .recurring(dueNode.get("recurring").asBoolean())
                    .dateTime(dueNode.get("datetime").asText())
                    .timezone(dueNode.get("timezone").asText())
                    .build();
        }

        return null;
    }

    private long[] deserializeLabelIds(JsonNode root) {
        JsonNode labelIdsJsonNode = root.get("label_ids");
        if (labelIdsJsonNode.isArray()) {
            Iterator<JsonNode> iterator = labelIdsJsonNode.iterator();
            return generate(() -> null)
                    .takeWhile(x -> iterator.hasNext())
                    .map(n -> iterator.next())
                    .mapToLong(JsonNode::asLong)
                    .toArray();
        }

        return new long[0];
    }
}
