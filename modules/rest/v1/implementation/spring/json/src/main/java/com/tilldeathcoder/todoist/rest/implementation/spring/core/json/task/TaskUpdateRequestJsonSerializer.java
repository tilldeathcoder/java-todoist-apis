package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.task;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tilldeathcoder.todoist.rest.api.task.model.TaskUpdateRequest;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.AbstractJsonDeserializer;

import java.io.IOException;

public class TaskUpdateRequestJsonSerializer extends AbstractJsonDeserializer<TaskUpdateRequest> {

    public TaskUpdateRequestJsonSerializer() {
        super(TaskUpdateRequest.class);
    }

    @Override
    public void serialize(TaskUpdateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        writeStringFieldIfNotEmpty(generator, "content", value.getContent());
        writeStringFieldIfNotEmpty(generator, "description", value.getDescription());
        writeLongArrayFieldIfNotNull(generator, "label_ids", value.getLabelIds());
        writeIntegerFieldIfNotNull(generator, "priority", value.getPriority());
        writeStringFieldIfNotEmpty(generator, "due_string", value.getDueString());
        writeStringFieldIfNotEmpty(generator, "due_date", value.getDueDate());
        writeStringFieldIfNotEmpty(generator, "due_datetime", value.getDueDateTime());
        writeStringFieldIfNotEmpty(generator, "due_lang", value.getDueLang());
        writeLongFieldIfNotNull(generator, "assignee", value.getAssignee());
        generator.writeEndObject();
    }
}
