package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.project;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tilldeathcoder.todoist.rest.api.project.model.ProjectCreateRequest;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.AbstractJsonDeserializer;

import java.io.IOException;

public class ProjectCreateRequestJsonSerializer extends AbstractJsonDeserializer<ProjectCreateRequest> {

    public ProjectCreateRequestJsonSerializer() {
        super(ProjectCreateRequest.class);
    }

    @Override
    public void serialize(ProjectCreateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeStringField("name", value.getName());
        writeLongFieldIfNotNull(generator, "parent_id", value.getParentId());
        writeLongFieldIfNotNull(generator, "parent", value.getParent());
        writeIntegerFieldIfNotNull(generator, "color", value.getColor());
        writeBooleanFieldIfNotNull(generator, "favorite", value.getFavorite());
        generator.writeEndObject();
    }
}
