package com.tilldeathcoder.todoist.rest.implementation.spring.core.json.label;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tilldeathcoder.todoist.rest.api.label.model.LabelUpdateRequest;
import com.tilldeathcoder.todoist.rest.implementation.spring.core.json.AbstractJsonDeserializer;

import java.io.IOException;

public class LabelUpdateRequestJsonSerializer extends AbstractJsonDeserializer<LabelUpdateRequest> {

    public LabelUpdateRequestJsonSerializer() {
        super(LabelUpdateRequest.class);
    }

    @Override
    public void serialize(LabelUpdateRequest value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        writeStringFieldIfNotEmpty(generator, "name", value.getName());
        writeBooleanFieldIfNotNull(generator, "favorite", value.getFavorite());
        writeIntegerFieldIfNotNull(generator, "order", value.getOrder());
        writeIntegerFieldIfNotNull(generator, "color", value.getColor());
        generator.writeEndObject();
    }
}
